<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array</title>
</head>
<body>
    <h1>Berlatih Array</h1>

    <?php
        echo "<h3> Soal 1 </h3>";

        // mengelompokkan nama-nama di bawah ini ke dalam Array.
        $matakuliah = ["sosiologi", "matematika", "psikologi", "tarbiyah"];
        print_r($matakuliah);

        echo "<h3> Soal 2<h3>";
        // tunjukan panjang Array di soal no 1 dan tampilkan isi dari masing-masing Array.

        echo "Mata Kuliah: ";
        echo "<br>";
        echo "Jumlah Mata Kuliah: ". count($matakuliah); // Berapa panjang array mata kuliah
        echo "<br>";
        echo "<ol>";
        echo "<li> $matakuliah[0] </li>";
        echo "<li> $matakuliah[1] </li>";
        echo "<li> $matakuliah[2] </li>";
        echo "<li> $matakuliah[3] </li>";
        // Lanjutkan
        echo "</ol>";

        // soal 3
        
        $biodata = [
            ["nama"=>"Edwin", "Kota" => "Palembang", "umur" => 24],
            ["nama"=>"Asep", "Kota" => "Bandung", "umur" => 24],
            ["nama"=>"Mona", "Kota" => "Makassar", "umur" => 24],
        ];
        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
    ?>
</body>
</html>