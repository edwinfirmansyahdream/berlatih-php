<!DOCTYPE html>
<?php
    $judul = "Contoh String PHP"
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas</title>
</head>
<body>
    <h1><?php echo $judul ?></h1>
    <?php
        echo "<h3> Soal No 1 </h3>";
        // mengelompokkan arrray
        $contoh_kalimat = "Hello World";

        echo "<p>Kalimat 1 = $contoh_kalimat</p>";
        echo "<p>Panjang Kalimat 1 = ". strlen($contoh_kalimat);
        echo "<p>jumlah kalimat 1 = ". str_word_count("$contoh_kalimat");

        echo "<h3> Nama </h3>";
        $contoh_kalimat2 = "Edwin Firmansyah Palembang";

        echo "<p>Kalimat 2 = $contoh_kalimat2</p>";
        echo "<p>Panjang Kalimat 2 = ". strlen($contoh_kalimat2);
        echo "<p>jumlah kalimat 2 = ". str_word_count($contoh_kalimat2) . "<br> <br>";

        // soal ke 2 mengambil kata pada string dan karakter-karakter yang ada di dalamnya.
        $string2 = "I love PHP";

        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ;

        echo "Kata kedua: " . substr($string2, 2, 4);
        echo "<br> Kata ketiga: " . substr($string2, 6, 8) .  "<br> <br>";

        // soal ketiga megubah karakter atua kata yang ada di dalam sebuah string.

        $string3 = "PHP is old but sexy!";
        $string3replace = str_replace("sexy", "awesome", $string3);
        echo "String: \"$string3\" ";
        echo "<br>";
        echo "String penganti : $string3replace";
    ?>
</body>
</html>